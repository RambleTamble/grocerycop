package models;

/**
 * Created by ivanenko on 26.01.2016.
 */
public class User {
    public String id;
    public String firstName;
    public String lastName;

    public User() {}

    public User(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }


}
