package controllers;

import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.users.*;

/**
 * Created by ivanenko on 26.01.2016.
 */
public class Users extends Controller {

    public static Result list() {
        String searchName = request().getQueryString("searchName");

        if (searchName == null || searchName.isEmpty()) {
            return ok(list.render(UserStorage.list(), Form.form(String.class)));
        } else {
            return ok(list.render(UserStorage.findByName(searchName), Form.form(String.class)));
        }

    }

    public static Result save() {
        DynamicForm requestData = Form.form().bindFromRequest();
        User newUser = UserFactory.createUser(requestData.get("firstName"), requestData.get("lastName"));
        UserStorage.put(newUser);
        return redirect(routes.Users.list());
    }

    public static Result newForm() {
        return ok(adduser.render(Form.form(User.class)));
    }
}
