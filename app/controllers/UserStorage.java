package controllers;

import models.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ivanenko on 26.01.2016.
 */
public class UserStorage {
    private static List<User> storage;

    static {
        storage = new ArrayList<User>();
        storage.add(new User("uid1", "aa", "bb"));
        storage.add(UserFactory.createUser("aac", "bbc"));
    }

    public static void put(User user) {
        storage.add(user);
    }

    public static List<User> findByName(String name) {
        if (name == null || "".equals(name.trim())) {
            return Collections.EMPTY_LIST;
        }

        List result = new ArrayList<User>();

        for (User user : storage) {
            if (name.equals(user.firstName) || name.equals(user.lastName) ||
                    name.equals(user.firstName + " " + user.lastName) ||
                    name.equals(user.lastName + " " + user.firstName)) {
                result.add(user);
            }
        }

        return result;
    }

    public static List<User> list() {
        return storage;
    }
}
