package controllers;

import models.User;

import java.util.UUID;

/**
 * Created by ivanenko on 26.01.2016.
 */
public class UserFactory {

    public static User createUser(String firstName, String lastName) {
        return new User(UUID.randomUUID().toString(), firstName, lastName);
    }
}
